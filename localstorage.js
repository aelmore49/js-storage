const storeBtn = document.getElementById('store-btn');
const retrieveBtn = document.getElementById('retrieve-btn');
const userId = 'u123';

const user = {
    name: 'Alex',
    age: 35,
    hobbies: ['Tennis', 'Website Development']
}

storeBtn.addEventListener('click', () => {
    sessionStorage.setItem('uid', userId);
    localStorage.setItem('user', JSON.stringify(user))
})

retrieveBtn.addEventListener('click', () => {
    const extractedId = sessionStorage.getItem('uid');
    const extractedUser = JSON.parse(localStorage.getItem('user'))
    console.log(`The user name is ${extractedUser.name}.`)
    if (extractedId) {
        console.log('Got the id - ' + extractedId);
    } else {
        console.log('Could not find the id')
    }
})